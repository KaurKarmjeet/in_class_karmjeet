<?php

use Illuminate\Database\Seeder;



class seed_cars_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
        	'name' => 'BMW',
             'description' =>'BMW is of black car',
             'image' => 'bmw.jpg'
         ]);

        DB::table('cars')->insert([
        	'name' => 'CMW',
             'description' =>'BMW is of red car',
             'image' => 'cmw.jpg'
         ]);

        DB::table('cars')->insert([
        	'name' => 'DMW',
             'description' =>'BMW is of white car',
             'image' => 'dmw.jpg'
         ]);
        DB::table('cars')->insert([
        	'name' => 'EMW',
             'description' =>'BMW is of black car',
             'image' => 'emw.jpg'
         ]);
        DB::table('cars')->insert([
        	'name' => 'FMW',
             'description' =>'BMW is of white car',
             'image' => 'fmw.jpg'
         ]);
    }
}
