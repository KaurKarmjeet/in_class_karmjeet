<?php

use Illuminate\Database\Seeder;

class seed_dogs_table extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('dogs')->insert([
        	'name'=>'Fido',
        	'description'=>'Fido is a blcak and white Puppy',
        	'image'=>'fido.jpg'
        ]);
        DB::table('dogs')->insert([
        	'name'=>'Lassie',
        	'description'=>'Lassie is a blcak and white Puppy',
        	'image'=>'lassie.jpg'
        ]);
        DB::table('dogs')->insert([
        	'name'=>'German',
        	'description'=>'German is a blcak and white Puppy',
        	'image'=>'german.jpg'
        ]);
        DB::table('dogs')->insert([
        	'name'=>'Pug',
        	'description'=>'Pug is a blcak and white Puppy',
        	'image'=>'pug.jpg'
        ]);
        DB::table('dogs')->insert([
        	'name'=>'American Bullu',
        	'description'=>'American Bully is a blcak and white Puppy',
        	'image'=>'american.jpg'
        ]);
    }
}
