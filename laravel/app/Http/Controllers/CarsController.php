<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cars;

class CarsController extends Controller
{
   public function index()
    {
      
        $cars = Cars::all();
        return view('karam.index',compact('cars'));
    }

    public function show($id)
     {

     	$car= Cars::find($id);
     	return view('karam.show',compact('car'));
     }
 

}
