@extends('narain.layout')

@section('content')
<h1>{{$plane->name}}</h1>
<div>
	<ul>
		<li><img width="200px" src="/images/{{ $plane->image }}"></li>
		<li>{{ $plane->name }}</li>
		<li>{{ $plane->description }}</li>
	</ul>
</div>
@endsection()