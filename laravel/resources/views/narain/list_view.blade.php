@extends('narain.layout')

@section('content')
<table class="table">
	<tr>
		<th>ID</th>
		<th>Image</th>
		<th>Name</th>
		<th>Description</th>

	</tr>
	@foreach($planes as $plane)
	<tr>
		<td>{{ $plane->id }}</td>
		<td><img width="150px" src="images/{{ $plane->image }}"></td>
		<td><a href="/planes/{{$plane->id}}">{{ $plane->name }}</a></td>
		<td>{{ $plane->description }}</td>

	</tr>
	@endforeach
</table>
@endsection()